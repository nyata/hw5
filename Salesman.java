/*
* 2014/07/04
* Travelling Salesman Problem
* Nagisa Yata
*/

import java.io.*;
import java.util.*;
import java.awt.geom.*;
import java.lang.Math;

class CompareAr implements Comparator {
	public int compare(Object o1, Object o2) {
		City n1 = (City)o1;
		City n2 = (City)o2;

		return n1.distance < n2.distance ? -1:1;
	}
}

class City {
	public float distance;
	public int node1;
	public int node2;

	public City(float dis, int n1, int n2) {
		this.distance = dis;
		this.node1 = n1;
		this.node2 = n2;
	}
}

class CityList {
	public int node1;
	public int node2;

	public CityList(int n1, int n2) {
		this.node1 = n1;
		this.node2 = n2;
	}
}

public class Salesman {
	private FileReader fr;
	private BufferedReader br;
	private String filePath = "/home/step/STEP/google-2014-step-tsp/input_0.csv";
	public ArrayList<Point2D.Float> pos;
	CompareAr compare = new CompareAr();
	int k1,k2;

	public static void main(String[] args) {
		Salesman sales = new Salesman();
		sales.solve();
	}

	public boolean equals(Object obj) {
		CityList city = (CityList)obj;
		if(city.node1 == k2 || city.node2 == k2) {
			return true;
		} else {
			return false;
		}
	}

	public void solve() {
		try {
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			String line;
			pos = new ArrayList<Point2D.Float>();
			Point2D.Float start, next;
			double diffX, diffY, distance, distance2, mindistance;
			int[] flg;
			int[][] flg3;
			boolean[] flg2;
			ArrayList<CityList> result;
			int[] result2;

			int pointer;
			City[] c;

			int i = 0;
			//load dict file
			while((line = br.readLine()) != null) {
				if(i != 0) { //ignore x,y
					String[] str = line.split(",");
					pos.add(new Point2D.Float(Float.valueOf(str[0]), Float.valueOf(str[1])));
				}
				i++;
			}

			c = new City[pos.size() * pos.size()];
			int k = 0;
			for(i = 0; i < pos.size(); i++) {
				start = pos.get(i);
				for(int j = 0; j < pos.size(); j++) {
					next = pos.get(j);
					diffX = next.x - start.x;
					diffY = next.y - start.y;
					distance = Math.sqrt(diffX * diffX + diffY * diffY);
					c[k] = new City((float)distance, i, j);
					k++;
				}
			}
			Arrays.sort(c, compare);

			flg = new int[pos.size()];
			flg3 = new int[pos.size()][pos.size()];
			flg2 = new boolean[pos.size()];
			result = new ArrayList<CityList>();
			distance = 0;
			int j;
			k1 = k2 = -1;
			i = j = 0;
			while(i < pos.size()) {
				City a = c[j];
				if(a.node1 != a.node2 || a.distance != 0.0) {
					if((flg[a.node1] < 2 && flg[a.node2] < 2) &&
					(flg3[a.node1][a.node2] < 1 && flg3[a.node2][a.node1] < 1)) {
						//System.out.println(a.node1 + "," + a.node2);
						result.add(new CityList(a.node1, a.node2));
						if(k != -1 && (a.node1 == 0 || a.node2 == 0)) {	
							if(a.node1 == 0) {
								k1 = i;
								k2 = a.node2;
							} else {
								k1 = i;
								k2 = a.node1;
							}
							flg2[0] = true;
							flg2[1] = true;
						}
						System.out.println(a.node1 + "," + a.node2);
						distance += a.distance;
						flg[a.node1] += 1;
						flg[a.node2] += 1;
						flg3[a.node1][a.node2] += 1;
						i++;
					}
				} 
				j++;
			}

			result2 = new int[pos.size()];
			result2[0] = 0;

			result2[pos.size() - 1] = -1;
			k = 2;
			i = 0;
			int index1, index2;
			index1 = index2 = -1;
			while(result2[4] < 0) {
				for(j = 0; j < pos.size(); j++) {
					index1 = result.indexOf(new CityList(3, 1));
					index2 = result.indexOf(new CityList(j, k2));
					if(0 <= index1) {
						System.out.println(index1);
						result2[k] = result.get(index1).node2;
						flg2[k] = true;
						k2 = result2[k];
						k++;
						break;
					} else if(0 <= index2) {
						result2[k] = result.get(index2).node1;
						flg2[k] = true;
						k2 = result2[k];	
						k++;
						break;
					}
				}			
			}

			for(int re : result2) {
				System.out.println(re);
			}

			System.out.println(distance);
		} catch(IOException e) {
			System.out.println("error");
		} finally {
			try {
				br.close();
				fr.close();
			} catch(IOException e) {
				System.out.println("error2");
			}
		}
	}
}
